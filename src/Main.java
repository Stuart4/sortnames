/**
 * Created by Jacob Stuart on 7/21/14.
 */
import java.util.LinkedList;
public class Main {

	public static void main(String[] args){
		LinkedList<String> names = new LinkedList<String>();
		for(String item: args){
			names.addLast(item);
		}
		System.out.println(sortLL(names));
	}

	public static boolean isAlphaOrder(String a, String b){
		a = a.toLowerCase();
		b = b.toLowerCase();
		if(a.charAt(0) < b.charAt(0))
			return true;
		else if (b.charAt(0) < a.charAt(0))
			return false;
		else if (a.equals(b))
			return true;
		else if(a.length() == 1 || b.length() == 1){
			if (a.length() == 1)
				return true;
			else return false;
		}
		else return isAlphaOrder(a.substring(1), b.substring(1));
	}

	public static LinkedList<String> sortLL(LinkedList<String> names){

		for (int i = 0; i < names.size() - 1; i++){
			if (!(isAlphaOrder(names.get(i), names.get(i+1)))) {
				String tmp;
				tmp = names.get(i);
				names.set(i, names.get(i+1));
				names.set(i+1, tmp);
				sortLL(names);
				break;
			}
		}
		return names;

	}
}
